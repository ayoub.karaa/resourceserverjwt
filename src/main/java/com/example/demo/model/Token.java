package com.example.demo.model;

import lombok.Data;

@Data
public class Token {

    private String access_token;
    private String id;
    private String userName;
    private String name;

}
